# Computational Multiphysics SuSe 23

Scripts and pages explaining my solutions to the exercises posed in Computational Multiphysics in Summer Semester 23. 

```
/
| README
| notebooks
  \ Julia Notebooks
| staticpages
  \ Static webpage exports of those same notebooks.
| pdfs
  \ PDF exports of those same notebooks.
\
```